### Hi there 👋, I'm Pedro Pombeiro

- 🔭 I'm currently working on CI Runner Fleet Visibility at GitLab, doing Ruby-on-Rails and PostgreSQL development on a very large SaaS/self-managed product.
- ⚡ Fun facts:
  - I got introduced to coding when I was around 8 years old as a means of loading games into a Sinclair ZX81.
  - I've been working in fully distributed teams since 2014, and worked in hybrid teams for 13 years before that.

Skills: Ruby on Rails / Go / C# / Linux / Nix / Bash / Lua

| Link | URL |
|------|-----|
| <img width=20px src="https://gitlab.com/uploads/-/system/project/avatar/17866971/68747470733a2f2f63646e2e7261776769742e636f6d2f64617669646f736f6d657468696e672f646f7466696c65732f6d61737465722f6d6574612f646f7466696c65732d6c6f676f2e706e67.png?width=20" /> Dotfiles repo | https://github.com/pedropombeiro/dotfiles |
| <img width=20px src="https://cdn.icon-icons.com/icons2/2368/PNG/512/github_logo_icon_143772.png" /> GitHub profile | https://github.com/pedropombeiro |
| 📫 LinkedIn profile | https://www.linkedin.com/in/pedropombeiro |

<!--
**pedropombeiro/pedropombeiro** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
